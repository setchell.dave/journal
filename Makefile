PREFIX := ${HOME}
PKG_NAME := journal
install_dir := ${PREFIX}/${PKG_NAME}
bin := $(wildcard bin/* bin/.*)
share := $(wildcard share/* share/.*)

test:
	@sh ./test.sh

$(install_dir):
	[ -d $@ ] || mkdir -p $@

$(share):
	cp -aR $(dir $@) ${install_dir}

$(bin):
	cp -aR $(dir $@) ${install_dir}

install: $(install_dir) $(share) $(bin)
	@echo installed to ${PREFIX}
    
.PHONY: install $(bin) $(share)
