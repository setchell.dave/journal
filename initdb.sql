CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE entry (
	id UUID PRIMARY KEY,
	init TIMESTAMP DEFAULT NOW(),
	name VARCHAR(256),
	deleted BOOLEAN DEFAULT FALSE
);

CREATE OR REPLACE FUNCTION log_entry() RETURNS TRIGGER AS $log$
    BEGIN
		IF (TG_OP = 'UPDATE') THEN
			INSERT INTO log (entry_id, action, description)
			SELECT NEW.id, 'update', TG_TABLE_NAME;
			RETURN NEW;
		ELSEIF (TG_OP = 'INSERT') THEN
			INSERT INTO log (entry_id, action, description)
			SELECT NEW.id, 'create', TG_TABLE_NAME;
			RETURN NEW;
        ELSEIF (TG_OP = 'DELETE') THEN
			INSERT INTO log (entry_id, action, description)
			SELECT OLD.id, 'delete', TG_TABLE_NAME;
            RETURN OLD;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$log$ LANGUAGE plpgsql;

CREATE TRIGGER log_engry
AFTER INSERT OR UPDATE OR DELETE ON entry
    FOR EACH ROW EXECUTE PROCEDURE log_entry();

CREATE TABLE text ( -- SCD for text
	id SERIAL PRIMARY KEY,
	entry_id UUID REFERENCES entry ON DELETE CASCADE,
	start TIMESTAMP DEFAULT NOW(),
	stop TIMESTAMP DEFAULT NULL,
	value TEXT DEFAULT NULL
);

CREATE TABLE meta ( -- SCD for meta
	id SERIAL PRIMARY KEY,
	entry_id UUID REFERENCES entry ON DELETE CASCADE,
	start TIMESTAMP DEFAULT NOW(),
	stop TIMESTAMP DEFAULT NULL,
	value JSONB DEFAULT NULL
);

CREATE OR REPLACE FUNCTION log_text_meta() RETURNS TRIGGER AS $log$
    BEGIN
		IF (TG_OP = 'UPDATE') THEN
			INSERT INTO log (entry_id, action, description)
			SELECT NEW.entry_id, 'update', TG_TABLE_NAME;
			RETURN NEW;
		ELSEIF (TG_OP = 'INSERT') THEN
			INSERT INTO log (entry_id, action, description)
			SELECT NEW.entry_id, 'create', TG_TABLE_NAME;
			RETURN NEW;
        ELSEIF (TG_OP = 'DELETE') THEN
			INSERT INTO log (entry_id, action, description)
			SELECT OLD.entry_id, 'delete', TG_TABLE_NAME;
            RETURN OLD;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$log$ LANGUAGE plpgsql;

CREATE TRIGGER log_text
AFTER INSERT OR UPDATE OR DELETE ON text
    FOR EACH ROW EXECUTE PROCEDURE log_text_meta();

CREATE TRIGGER log_meta
AFTER INSERT OR UPDATE OR DELETE ON meta
    FOR EACH ROW EXECUTE PROCEDURE log_text_meta();

CREATE TABLE assoc_entry (
	id SERIAL PRIMARY KEY,
	init TIMESTAMP DEFAULT NOW(),
	a UUID REFERENCES entry ON DELETE CASCADE NOT NULL,
	b UUID REFERENCES entry ON DELETE CASCADE NOT NULL,
	name VARCHAR(256),
	weight INTEGER DEFAULT 0,
	UNIQUE(a,b)
);

CREATE TABLE timespan (
	id SERIAL PRIMARY KEY,
	init TIMESTAMP DEFAULT NOW(),
	name VARCHAR(256),
	description TEXT,
	entry_id UUID REFERENCES entry ON DELETE CASCADE NOT NULL,
	start TIMESTAMP DEFAULT NULL,
	deadline TIMESTAMP DEFAULT NULL,
	stop TIMESTAMP DEFAULT NULL
);

CREATE OR REPLACE FUNCTION log_timespan() RETURNS TRIGGER AS $log$
    BEGIN
		IF (TG_OP = 'UPDATE') THEN
			INSERT INTO log (entry_id, timespan_id, action, description)
			SELECT NEW.entry_id, NEW.id, 'update', TG_TABLE_NAME;
			RETURN NEW;
		ELSEIF (TG_OP = 'INSERT') THEN
			INSERT INTO log (entry_id, timespan_id, action, description)
			SELECT NEW.entry_id, NEW.id, 'create', TG_TABLE_NAME;
			RETURN NEW;
        ELSEIF (TG_OP = 'DELETE') THEN
			INSERT INTO log (entry_id, timespan_id, action, description)
			SELECT OLD.entry_id, OLD.id, 'delete', TG_TABLE_NAME;
            RETURN OLD;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$log$ LANGUAGE plpgsql;

CREATE TRIGGER log_timespan
AFTER INSERT OR UPDATE OR DELETE ON timespan
    FOR EACH ROW EXECUTE PROCEDURE log_timespan();

CREATE TYPE media_type AS ENUM ('video', 'audio', 'image', 'archive', 'document');

CREATE TABLE media (
	id OID PRIMARY KEY,
	init TIMESTAMP DEFAULT NOW(),
	name VARCHAR(256),
	description TEXT,
	source VARCHAR(256),
	type MEDIA_TYPE
);

CREATE TABLE assoc_media (
	id SERIAL PRIMARY KEY,
	init TIMESTAMP DEFAULT NOW(),
	a UUID REFERENCES entry ON DELETE CASCADE NOT NULL,
	b OID REFERENCES media ON DELETE CASCADE NOT NULL,
	UNIQUE(a,b)
);

CREATE OR REPLACE FUNCTION log_media() RETURNS TRIGGER AS $log$
    BEGIN
		IF (TG_OP = 'UPDATE') THEN
			INSERT INTO log (entry_id, media_id, action, description)
			SELECT NEW.a, NEW.b, 'update', TG_TABLE_NAME;
			RETURN NEW;
		ELSEIF (TG_OP = 'INSERT') THEN
			INSERT INTO log (entry_id, media_id, action, description)
			SELECT NEW.a, NEW.b, 'create', TG_TABLE_NAME;
			RETURN NEW;
        ELSEIF (TG_OP = 'DELETE') THEN
			INSERT INTO log (entry_id, media_id, action, description)
			SELECT OLD.a, OLD.b, 'delete', TG_TABLE_NAME;
            RETURN OLD;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$log$ LANGUAGE plpgsql;

CREATE TRIGGER log_media
AFTER INSERT OR UPDATE OR DELETE ON assoc_media
    FOR EACH ROW EXECUTE PROCEDURE log_media();

CREATE TYPE action AS ENUM ('create', 'update', 'delete');
CREATE TABLE log (
	id SERIAL PRIMARY KEY,
	init TIMESTAMP DEFAULT NOW(),
	entry_id UUID REFERENCES entry ON DELETE CASCADE NOT NULL,
	timespan_id INTEGER REFERENCES timespan ON DELETE CASCADE DEFAULT NULL,
	media_id INTEGER REFERENCES media ON DELETE CASCADE DEFAULT NULL,
	action ACTION,
	description TEXT
);


-- CREATING VIEWS MA DOODS
-- id, init, name, deleted, text, modify_time, meta, meta_modify_time
CREATE OR REPLACE VIEW entry_v AS
	SELECT e.*, text.value as text, text.start as modify_time, 
		meta.value as meta, meta.start as meta_modify_time
	FROM entry as e
	LEFT JOIN text ON e.id = text.entry_id AND text.stop IS NULL
	LEFT JOIN meta ON e.id = meta.entry_id AND meta.stop IS NULL;

-- id, link, depth, id_path, name_path, cycle
CREATE OR REPLACE RECURSIVE VIEW graph_v(id, link, depth,
		id_path, name_path, cycle) AS (
	SELECT e.id, assoc_entry.b, 1,
		ARRAY[e.id],
		-- THINK technically don't need id and name
		ARRAY[e.name]::text[],
		false
	FROM entry_v e
	LEFT JOIN assoc_entry ON e.id = assoc_entry.a 
	UNION ALL
	SELECT e.id, assoc_entry.b, sg.depth + 1,
		sg.id_path || e.id,
		array_append(sg.name_path, e.name::text),
		e.id = ANY(id_path)
	FROM entry_v e
	LEFT JOIN assoc_entry ON e.id = assoc_entry.a 
	JOIN graph_v AS sg ON e.id = sg.link
	WHERE NOT cycle
);

-- id, init, name, deleted, text, modify_time, meta, meta_modify_time
-- time_id, time_name, time_description, time_init,
-- time_start, time_stop, time_deadline
CREATE OR REPLACE VIEW entry_time_v AS
	SELECT e.*, t.id as time_id, t.name as time_name,
		t.description as time_description, t.init as time_init,
		t.start AS time_start, t.stop AS time_stop, t.deadline AS time_deadline
	FROM entry_v as e
	LEFT JOIN timespan AS t ON e.id = t.entry_id;

-- id, init, name, deleted, text, modify_time, meta, meta_modify_time
-- media_id, media_name, media_description, media_init
-- media_source, media_type
CREATE OR REPLACE VIEW entry_media_v AS
	SELECT e.*, m.id as media_id, m.name as media_name,
		m.description as media_description, m.init AS media_init,
		m.source as media_source, m.type AS media_type
	FROM entry_v as e
	LEFT JOIN assoc_media ON e.id = assoc_media.a
	JOIN media AS m ON assoc_media.b = m.id;
	
-- id, init, name, deleted, text, modify_time, meta, meta_modify_time
-- time_id, time_name, time_description, time_init,
-- time_start, time_stop, time_deadline
-- media_id, media_name, media_description, media_init
-- media_source, media_type
-- id_path, path, link_name, link_weight, depth
CREATE OR REPLACE VIEW entry_all AS
	SELECT t.*, m.media_id, m.media_name, m.media_description, m.media_init,
		m.media_source, m.media_type, g.id_path, array_to_string(g.name_path, '/') as path,
		a.name AS link_name, a.weight AS link_weight, g.depth
	FROM entry_v AS e
	LEFT JOIN entry_time_v as t ON e.id = t.id
	LEFT JOIN entry_media_v as m ON e.id = m.id
	JOIN graph_v as g ON e.id = g.id
	LEFT JOIN assoc_entry AS a ON a.a = g.id_path[array_upper(g.id_path, 1) - 1]
		AND a.b = g.id_path[array_upper(g.id_path, 1)]
	
