cat >$tmp/split.m4 <<'EOF'
	mpat="$1"; shift
	c=$(echo "$mpat" | cut -c1)
	case $c in [0-9a-zA-Z]) delim='M4_1';; *) delim=$c; mpat="${mpat#?}" ;; esac
	OIFS="$IFS"; IFS="M4_1"; set -f; set -- $mpat; IFS="$OIFS"; set +o noglob
EOF
<$tmp/split.m4 _form ',' >$tmp/split.comma
<$tmp/split.m4 _form ':' >$tmp/split.sub


_match_pattern () { # [deleted] -> match_pattern(+|-)[match_pattern(+|-)...] -> ids
	. $tmp/split.comma
	FIRST=true
	while test -n "$1"
	do
		(. $tmp/split.sub
		NEW=false; OP=UNION
		case "$1" in
		e) OP=EXCEPT; shift ;;
		i) OP=INTERSECT; shift ;;
		u) OP=UNION; shift ;; # explicit, not needed
		n) NEW=true; shift ;;
		esac
		$FIRST || echo $OP
		echo -n '('
		if $NEW
		then 
			id=$(_add_entry_fmt "$@" | _run_fmt2)
			_match_field_fmt "id" "$id" '=' $view
		else
			case "$1" in 
			????????-????-????-????-????????????) _match_field_fmt id "$1" '=' $view ;;
			*) _match_field_fmt "${1}" "$2" "$3" $view ;;
			esac
		fi)
		# TODO kinda needs an option, so meh
		echo "AND deleted = ${DELETED:-false})"
		shift; FIRST=false
	done
}

_add_entry_fmt () { # name -> data -> id
	if test -f "$2"
	then t=$(<"$2" base64 -w0)
	else t=$(echo -n "$2" | base64 -w0)
	fi
	if test -f "$3"
	then m=$(<"$3" base64 -w0)
	elif test -z "$3"
	then m=$(echo '{}' | base64 -w0)
	else m=$(echo -n "$3" | base64 -w0)
	fi; 
	_form "$1" "$t" "$m" <<EOF
WITH e as (
	INSERT INTO entry (id, name)
	VALUES (gen_random_uuid(), 'M4_1')
	RETURNING id),
	_ as (
	INSERT into meta (entry_id, value)
	SELECT e.id, convert_from(decode('M4_3', 'base64'), 'UTF8')::jsonb
	FROM e)
INSERT INTO text (entry_id, value)
	SELECT e.id, convert_from(decode('M4_2', 'base64'), 'UTF8')
	FROM e
RETURNING entry_id
EOF
}

_set_text_fmt () { # idset -> data -> ()
	if test -f "$2"
	then t=$(<"$2" base64 -w0)
	else t=$(echo -n "$2" | base64 -w0)
	fi
	_form "$1" "$t" <<EOF
WITH t AS (SELECT entry_id FROM text
		   WHERE entry_id in (M4_1) AND stop IS NULL),
	 _ AS (UPDATE text SET stop = NOW() WHERE entry_id IN (SELECT entry_id FROM t))
INSERT INTO text (entry_id, value)
SELECT t.entry_id, convert_from(decode('M4_2', 'base64'), 'UTF8') FROM t
EOF
}

_set_meta_fmt () {
	if test -f "$2"
	then m=$(<"$2" base64 -w0)
	elif test -z "$2"
	then m=$(echo '{}' | base64 -w0)
	else m=$(echo -n "$2" | base64 -w0)
	fi; 
	_form "$1" "$m" <<EOF
WITH m AS (SELECT entry_id FROM meta
		   WHERE entry_id in (M4_1) AND stop IS NULL),
	 _ AS (UPDATE meta SET stop = NOW() WHERE entry_id IN (SELECT entry_id FROM m))
INSERT INTO meta (entry_id, value)
SELECT m.entry_id, convert_from(decode('M4_2', 'base64'), 'UTF8')::jsonb FROM m
EOF
}

_app_text_fmt () { # data -> ?
	if test -f "$2"
	then t=$(<"$2" base64 -w0)
	else t=$(echo -n "$2" | base64 -w0)
	fi
	_form "$1" "$t" <<EOF
WITH t AS (SELECT entry_id, value FROM text
		   WHERE entry_id in (M4_1) AND stop IS NULL),
	 _ AS (UPDATE text SET stop = NOW() WHERE entry_id IN (SELECT entry_id FROM t))
INSERT INTO text (entry_id, value)
SELECT t.entry_id, t.value || convert_from(decode('M4_2', 'base64'), 'UTF8') FROM t
EOF
}

_read_field_fmt () { # idset -> field -> [match op] -> text
	(_form "$1" "$2" $view <<EOF
SELECT DISTINCT M4_2 FROM M4_3
WHERE id IN (M4_1)
EOF
	shift; shift
	if test -n "$1" 
	then
		. $tmp/split.comma
		while test -n "$1"
		do
			(. $tmp/split.sub
			_form "$1" "$2" "${3:-~}" <<EOF
AND M4_1 M4_3 'M4_2'
EOF
			)
			shift
		done
	fi)
}

_set_field_fmt () { #  field -> value -> table -> ()
	_form "$1" "$2" "$3" "$4" <<EOF
UPDATE M4_4 SET
M4_2 = 'M4_3'
WHERE id IN (M4_1)
EOF
}

_set_link_fmt () {
	_form "$@" <<'EOF'
UPDATE assoc_entry SET
M4_3 = coalesce('M4_4', assoc_entry.`'M4_3)
WHERE assoc_entry.a IN (M4_1) AND assoc_entry.b IN (M4_2) 
EOF
}

_ins_link_fmt () { # id -> ?
	_form "$1" "$2" "$3" "${4:-0}" <<'EOF'
INSERT INTO assoc_entry (a,b,name,weight)
SELECT x.id, y.id, coalesce('M4_3', NULL), M4_4
FROM (M4_1) AS x, (M4_2) as y
ON CONFLICT DO NOTHING
EOF
}

_add_timespan_fmt () { # entryid -> name -> description -> start? -> ()
	d=$(echo -n "$3" | base64 -w0)
	_form "$1" "$2" "$d" "$4" <<EOF
INSERT INTO timespan (entry_id, name, description, start)
SELECT e.id, 'M4_2', convert_from(decode('M4_3', 'base64'), 'UTF8'),
	(CASE WHEN 'M4_4' = '' THEN NULL ELSE 'M4_4' END)::TIMESTAMP
FROM (M4_1) as e
EOF
}

_swap_history_fmt () {
	_form "$1" ${2:-1} <<EOF
WITH og AS (SELECT id FROM data WHERE entry_id = 'M4_1' AND stop IS NULL),
	new AS (SELECT id, ROW_NUMBER() OVER (ORDER BY stop DESC) FROM data
			WHERE entry_id = 'M4_1' AND stop IS NOT NULL),
	_ AS (UPDATE data SET stop = NULL WHERE id = (SELECT id FROM new WHERE row_number = M4_2))
UPDATE data SET stop = NOW() FROM og WHERE data.id = og.id 
EOF
}

_delete_fmt () { # () -> ()
	_form "$1" "$2" <<EOF
UPDATE entry SET
deleted = TRUE
WHERE id in (M4_1)
EOF
}

_undelete_fmt () { # () -> ()
	_form "$1" "$2" <<EOF
UPDATE entry SET
deleted = FALSE
WHERE id in (M4_1)
EOF
}

_match_field_fmt () { # field -> value -> operator -> ()
	_form "$1" "$2" "${3:-~}" "$4" <<EOF
SELECT id FROM M4_4
WHERE M4_1 M4_3 'M4_2'
EOF
}

_get_roots_fmt () { # () -> root ids
	_form <<EOF
SELECT id FROM entry
WHERE id NOT IN
(SELECT b FROM assoc_entry)
AND NOT deleted
EOF
}

_get_leaves_fmt () { # () -> leaf ids
	_form <<EOF
SELECT id FROM entry
WHERE id NOT IN
(SELECT a FROM assoc_entry)
AND NOT deleted
EOF
}

_get_solo_fmt () { # () -> solo ids
	_form <<EOF
SELECT id FROM entry
WHERE id NOT IN
(SELECT a FROM assoc_entry
 UNION SELECT b FROM assoc_entry)
AND NOT deleted
EOF
}

_order_fmt () { # idset -> field -> limit -> table -> ids
	table="$4"; test "$table" = entry -o -z "$table" && table=entry_v
	_form "$1" "$2" "$3" "$table" <<EOF
SELECT id FROM M4_4
WHERE NOT deleted AND id IN (M4_1)
ORDER BY M4_2 DESC LIMIT M4_3
EOF
}

_json_entry_fmt () {  # fieldnames -> [fieldvalues in json]
	# TODO fixup args
	_form <<EOF
SELECT row_to_json(row)
FROM ($(_read_entry_fmt "${1:-*}")) row
EOF
}

_ins_media_fmt () {
	_form "$1" "$2" <<'EOF'
INSERT INTO assoc_media (a,b)
SELECT x.id, 'M4_2'
FROM (M4_1) AS x
ON CONFLICT DO NOTHING
EOF
}

_rem_assoc_fmt () { # id -> ?
	_form "$1" "$2" ${3:-entry} <<'EOF'
DELETE FROM assoc_`'M4_3
WHERE a = 'M4_2'
AND b IN (M4_1)
EOF
}

_list_children_fmt () { # () -> ids
	_form "$1" ${2:-entry} <<'EOF'
SELECT b FROM assoc_`'M4_2
WHERE a IN (M4_1)
AND b IN (SELECT id FROM M4_2 WHERE NOT deleted)
EOF
}

_list_parents_fmt () { # () -> ids
	_form "$1" ${2:-entry} <<'EOF'
SELECT a FROM assoc_`'M4_2
WHERE b IN (M4_1)
AND a IN (SELECT id FROM M4_2 WHERE NOT deleted)
EOF
}

_add_media_fmt () { # oid -> name -> source -> type -> description? -> ??
	_form "$1" "$2" "$3" "$4" "$5" <<'EOF'
INSERT INTO media (id, name, source, type)
VALUES ('M4_1', 'M4_2', $_Q_$M4_3$_Q_$, 'M4_4')
RETURNING id
EOF
}

_load_oid () { # name -> file -> url -> type -> Monad oid-stored
	oid=$(printf "%s '%s';\n" '\lo_import' "$2" | psql $_P | cut -d' ' -f2)
	_add_media_fmt "$oid" "$1" "$3" "$4" | _run_fmt2
}

_export_oid () { # oid -> target file -> Monad written file
	printf "%s %s '%s';\n" '\lo_export' "$1" "$2" | psql $_P
}

_run_fmt2 () {
	sed '$a;\n' | psql $POPTS $_P
}
_run_fmt () {
	"$@" | sed '$a;\n' | psql $POPTS $_P
}

_edit_data () {
	input="$1"; l=$(<"$input" wc -l); test -f "$input" || return 1
    test $l -gt 5 && { _ask "edit $l files; are you sure?" || return 0 ;}
    (cd "$tmp"
    for i in $(cat "$input")
    do
		i=$(printf "'%s'" "$i")
		name=$(_run_fmt _read_entry_fmt "$i" name) 
		_run_fmt _read_entry_fmt "$i" data >"$name"
		</dev/tty $E "$name" >/dev/tty 2>/dev/tty
		data=$(cat "$name")
		_run_fmt _upd_entry_fmt "$i" data "$data"
    done
    )
}

_make_id_set () {
	tf=$(mktemp -p $tmp)
	cat - >$tf
	if test -s "$tf"
	then
		<"$tf" xargs -n1 printf ",'%s'" | cut -c2-
	else
		echo NULL
	fi
}

