journal
-------

## entry

	create
	order [name,init,modify_time]
	match on [name,data,meta,init,modify_time,tag]
	roots/leaves/solo/all
	list/add/remove children [entry,schedule,media,tag]
	list parents [entry]
	read [name,data,meta,init,modify_time]
	set [name,data,meta]
	append [data]
	delete/undelete
	browse/prev/next [data,meta]
## timespan

	create
	list
	set [name,description]
	match [init,started,stopped,name,description]
	read [name,description]
	delete/undelete
	list entry
	order

## media

	create ( import )
	list
	read [name,description,source,init,type]
	export
	set [name,description,type]
	match [init,source,name,description,type]
	delete/undelete
	list entry


